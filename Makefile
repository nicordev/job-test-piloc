ifdef class
	SEEDER = --class=${class}
endif

install: .composer-install .init-jwt migrate seed

.composer-install:
	composer install

.init-jwt:
	php artisan vendor:publish --provider="PHPOpenSourceSaver\JWTAuth\Providers\LaravelServiceProvider"
	php artisan jwt:secret

serve:
	php artisan serve

model:
	php artisan make:model ${model} --migration

migrate:
	php artisan migrate

cache-clear:
	php artisan cache:clear

cc: .clear-cache

test:
	php artisan test $(FILTER)

migrate:
	php artisan migrate:fresh

seed:
	php artisan db:seed $(SEEDER)

database: migrate seed
