<?php

namespace Database\Factories;

use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Property>
 */
class PropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'label' => fake()->unique->word(),
            'street' => fake()->unique()->streetAddress(),
            'zipcode' => fake()->numberBetween(10000, 99000),
            'city' => fake()->city(),
            'surface' => fake()->numberBetween(100, 10000),
            'rent_price' => fake()->numberBetween(50, 5000),
            'status' => Property::STATUSES[array_rand(Property::STATUSES)],
            'owner_id' => fake()->numberBetween(1, 11),
        ];
    }
}
