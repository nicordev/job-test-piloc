# Piloc test

## Installation

> You need php 8 installed and a database.

configure your database credentials in `.env` file.

run:

```bash
make install
```

## Usage

```bash
make serve
```
