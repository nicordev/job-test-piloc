<?php

namespace Tests\Feature;

use App\Models\Property;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    /**
     * @return void
     */
    public function test_index_is_successful(): void
    {
        $response = $this->get('/api/properties');

        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function test_index_returns_paginated_properties(): void
    {
        $response = $this->get('/api/properties');

        $content = json_decode($response->getContent(), true);
        self::assertNotEmpty($content);
        self::assertArrayHasKey('data', $content);
        self::assertCount(10, $content['data']);
    }

    /**
     * @return void
     */
    public function test_show_returns_property(): void
    {
        $response = $this->get('/api/properties/2');

        $content = json_decode($response->getContent(), true);
        self::assertNotEmpty($content);
        self::arrayHasKey('id', $content);
        self::assertSame(2, $content['id']);
    }

    /**
     * @return void
     */
    public function test_store_saves_property(): void
    {
        $response = $this->post('/api/properties', [
            'label' => 'dummy label',
            'street' => 'dummy street',
            'zipcode' => '7777',
            'city' => 'dummy city',
            'surface' => 777,
            'rent_price' => 77,
            'status' => Property::STATUS_AVAILABLE,
            'owner_id' => 2,
        ]);

        $response->assertStatus(201);
    }
}
