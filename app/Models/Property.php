<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    public const STATUS_AVAILABLE = 'available';
    public const STATUS_RENTED = 'rented';
    public const STATUSES = [
        self::STATUS_AVAILABLE,
        self::STATUS_RENTED,
    ];

    protected $fillable = [
        'label',
        'street',
        'zipcode',
        'city',
        'surface',
        'rent_price',
        'status',
        'owner_id',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
